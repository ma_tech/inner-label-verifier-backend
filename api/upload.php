<?php


include "../connection/connection.php";
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Access-Control-Allow-Origin: *');

$input=file_get_contents("php://input");
$decode=json_decode($input, true);

$secretKey = $decode['secretKey'];
if(!isset($secretKey)){
    echo '{"message":"unauthorized"}';
    
    // header('location:http://google.com');
    return;
}else {
    if($secretKey !== "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55IjoiTUFUZWNoIiwiZGV2ZWxvcGVyIjoiU2hlcndpbiBCYXV0aXN0YSIsImVtcElEIjoxMDk5MH0.uel5E-cu8H-AKBzP5kqMNwjc65VaDHo90hl31CgVgjY"){
        echo '{"message":"unauthorized"}';
        // header('location:http://google.com');
        
        return;
    }
}
// $qrData = array();
// $qrData = $decode['qrData'];
$qrData = $decode['qrData'];
$credentials = $decode['credentials'];

$EMPNUM = $credentials[0]['EMP_NUM'];
$EMP_FULLNAME = $credentials[0]['FULL_NAME'];
$UPDATED_PRODUCT_NAME = $qrData[0]['UPDATED_PRODUCT_NAME'];
$GOODS_CODE = $qrData[0]['GOODS_CODE'];
$MANUFACTURER_NAME = $qrData[0]['MANUFACTURER_NAME'];
$ASSY_LINE = $qrData[0]['ASSY_LINE'];
$PART_NAME = $qrData[0]['PART_NAME'];
$ITEM_CODE = $qrData[0]['ITEM_CODE'];
$PART_NUMBER = $qrData[0]['PART_NUMBER'];
date_default_timezone_set('Asia/Manila');
$currentDateTime = date('Y-m-d H:i:s');
$LOT_NUMBER = $qrData[1]['LOT_NUMBER'];
$resultArray = array();
// echo json_encode(array("message" => $credentials));
// return;



    $insertQuery = "INSERT INTO [MA_Receiving].[dbo].[INNER_LABEL_SCAN_DETAILS] (UPDATED_PRODUCT_NAME, GOODS_CODE, MANUFACTURER_NAME, ASSY_LINE, PART_NAME, ITEM_CODE, PART_NUMBER,SCAN_DATE, SCAN_BY, SCAN_PERSON, LOT_NUMBER)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
    $params = array(
        $UPDATED_PRODUCT_NAME, 
        $GOODS_CODE, 
        $MANUFACTURER_NAME, 
        $ASSY_LINE, 
        $PART_NAME, 
        $ITEM_CODE,
        $PART_NUMBER,
        $currentDateTime,
        $EMPNUM,
        $EMP_FULLNAME,
        $LOT_NUMBER);

    $stmt = sqlsrv_query( $conn, $insertQuery, $params);
    // sleep(5);
    // echo json_encode(array("message" => $stmt));
    // return;
    if ($stmt === false) {
        // Handle error if the query execution fails
        echo json_encode(array("message" => "fail"));
    } else {
        // Query executed successfully
        // Perform any additional actions or logic here
        echo json_encode(array("message" => "success"));
    }

sqlsrv_close($conn);


